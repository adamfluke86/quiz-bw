describe("Data", function () {
    var data;

    beforeEach(function () {
        data = new Data();
    });

    describe('initialization', function () {
        it("should have an empty repos array", function () {
            expect(data.repos).toEqual([]);
        });

        it("should set the current page to 1", function () {
            expect(data.page).toEqual(1);
        });
    });

    describe('.setRepos', function () {
        var repos;
        beforeEach(function () {
            repos = [
                { id: 1, contributors_url: 'contribUrl' }
            ]
        });

        it("should set the repos array to passed in array", function () {
            data.setRepos(repos);
            expect(data.repos).toBe(repos);
        });
    });

    describe('.setTopContributor', function () {
        var repos, contributor;
        beforeEach(function () {
            repos = [
                { id: 1, contributors_url: 'contribUrl' }
            ];

            contributor = { id: 2 };

            data.repos = repos;
        });

        it("should set the top contributor when given index value and contributor", function () {
            data.setTopContributor(contributor, { index: 0 });
            expect(data.repos[0].topContributor).toBe(contributor);
        });
    });

    describe('.currentRepos', function () {
        var repos;
        beforeEach(function () {
            repos = [
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' },
                { id: 1, contributors_url: 'contribUrl' }
            ];

            data.repos = repos;
        });

        it("should return 10 from the correct page", function () {
            console.log(data.getCurrentRepos());
            expect(data.getCurrentRepos().length).toBe(10);
            expect(data.getCurrentRepos()[0]).toBe(repos[0]);
            expect(data.getCurrentRepos()[9]).toBe(repos[9]);
            data.page = 2;
            expect(data.getCurrentRepos().length).toBe(10);
            expect(data.getCurrentRepos()[0]).toBe(repos[10]);
            expect(data.getCurrentRepos()[9]).toBe(repos[19]);
        });
    });
});
