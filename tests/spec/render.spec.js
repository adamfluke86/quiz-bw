describe("Render", function () {
    var render;

    beforeEach(function () {
        render = new Render();
    });

    describe('initialization', function () {
    });

    // Note: I may not decide to test much in the render service. Generally, I don't find it time efficient
    // to try and test dom modification in a unit test, and the render service shouldn't be doing much calculation
    // or modification of anything.
});
