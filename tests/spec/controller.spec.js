describe("AppController", function () {
    var ctrl, GitHubWrapper, Render, Data;

    beforeEach(function () {
        GitHubWrapper = {
            get: function () {
            }
        };

        Render = {
            updateFromData: function () {
            }
        };

        Data = {
            setRepos: function () {},
            setTopContributor: function () {}
        };

        ctrl = new AppController(GitHubWrapper, Render, Data);
    });

    describe('.getData', function () {
        var promise, response;
        beforeEach(function () {
            response = ["repos"];
            promise = new Promise(function(resolve){ resolve(response)});
            spyOn(GitHubWrapper, 'get').and.returnValue(promise);
            spyOn(Render, 'updateFromData').and.returnValue(true);
            spyOn(ctrl, 'getContributorData').and.returnValue(true);
            spyOn(Data, 'setRepos').and.returnValue(true);
        });

        it("should call GitHubWrapper.get", function () {
            ctrl.getData();
            expect(GitHubWrapper.get).toHaveBeenCalledWith({repos: true});
        });

        it("should take the resolved resource and set it as the data property", function (done) {
            ctrl.getData();
            promise.then(function(){
                expect(Data.setRepos).toHaveBeenCalledWith(response);
                done();
            });
        });

        it("should call the render with ctrl.data on resolve", function (done) {
            ctrl.getData();
            promise.then(function(){
                expect(Render.updateFromData).toHaveBeenCalledWith(Data);
                done();
            });
        });
    });

    describe('.getContributorData', function () {
        var promise, response;
        beforeEach(function () {
            response = { id: '1'};
            promise = new Promise(function(resolve){ resolve(response)});
            spyOn(GitHubWrapper, 'get').and.returnValue(promise);
            spyOn(Render, 'updateFromData').and.returnValue(true);
            spyOn(Data, 'setTopContributor').and.returnValue(true);

            ctrl.data.repos = [
                { id: 1, contributors_url: 'contribUrl' }
            ];
        });

        it("should call GitHubWrapper.get for each repo", function () {
            ctrl.getContributorData();
            expect(GitHubWrapper.get).toHaveBeenCalledWith({ topContributor: true, contribUrl: "contribUrl"});
        });

        it("should take the resolved contributor and set it as the contributor data property", function (done) {
            ctrl.getContributorData();
            promise.then(function(){
                expect(Data.setTopContributor).toHaveBeenCalledWith(response, { index: 0 });
                done();
            });
        });

        it("should call the render with ctrl.data on resolve", function (done) {
            ctrl.getContributorData();
            promise.then(function(){
                expect(Render.updateFromData).toHaveBeenCalledWith(Data);
                done();
            });
        });
    });
});
