describe("GitHubWrapper", function () {
    describe('.get', function () {
        var promise, response;
        beforeEach(function () {
            response = { items: [] };
            promise = new Promise(function(resolve){ resolve(response)});
            spyOn($, 'get').and.returnValue(promise);
        });

        describe('config = { repos: true }', function () {
            it('calls get on the repo search endpoint', function () {
                GitHubWrapper.get({repos: true});
                expect($.get).toHaveBeenCalledWith("https://api.github.com/search/repositories?q=stars:>=15000&sort=stars&order=desc");
            });

            // it('resolves the repos array when get promise resolves', function (done) {
                // TODO: Figure out how to resolve these promises in the correct order
                // var call = GitHubWrapper.get({repos: true});
                // promise.then(function(){
                //     setTimeout(function () {
                //         call.then(function(){
                //             setTimeout(function () {
                //                 expect(call.isFulfilled()).toBeTruthy();
                //                 done();
                //             }, 0);
                //         });
                //     }, 0);
                // });
            // });
        });

        describe('config = { topContributor: true, contribUrl: "" }', function () {
            it('calls get on the repo contributor list endpoint', function () {
                GitHubWrapper.get({topContributor: true, contribUrl: "https://api.github.com/repos/FreeCodeCamp/FreeCodeCamp/contributors"});
                expect($.get).toHaveBeenCalledWith("https://api.github.com/repos/FreeCodeCamp/FreeCodeCamp/contributors");
            });
        });
    });
});
