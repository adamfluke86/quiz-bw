/**
 * Created by adam on 10/15/16.
 *
 * AppController
 */

function AppController (GitHubWrapper, Render, Data) {
    this.GitHubWrapper = GitHubWrapper;
    this.Render = Render;
    this.data = Data;
}

AppController.prototype.getData = function () {
    var ctrl = this;
    this.GitHubWrapper.get({ repos: true })
                        .then(function(resp){
                            ctrl.data.setRepos(resp);
                            ctrl.Render.updateFromData(ctrl.data);
                        })
                        .then(function () {
                            ctrl.getContributorData();
                        });
};

AppController.prototype.getContributorData = function () {
    var ctrl = this;
    ctrl.data.repos.forEach(function(repo, index){
        ctrl.GitHubWrapper.get({ topContributor: true, contribUrl: repo["contributors_url"] })
            .then(function (contributor) {
                ctrl.data.setTopContributor(contributor, { index: index });
                ctrl.Render.updateFromData(ctrl.data);
            });
    });
};

AppController.prototype.bindControls = function () {
    var ctrl = this;
    $('#prev').on('click', function(){
        ctrl.data.shiftPage(-1);
        ctrl.Render.updateFromData(ctrl.data);
    });
    $('#next').on('click', function(){
        ctrl.data.shiftPage(1);
        ctrl.Render.updateFromData(ctrl.data);
    });
};