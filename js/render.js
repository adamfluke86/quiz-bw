/**
 * Created by adam on 10/19/16.
 */
function Render () {
}

Render.prototype.updateFromData = function (data) {
    var render = this;
    if (!render.currentData) {
        render.currentData = this;
        $('#loading').addClass('hidden');
        $('#controls').removeClass('hidden');
        $.each($('.repo'), function(index, repo){
           $(repo).removeClass('hidden');
        });
    }

    updateRepos(data.getCurrentRepos(), data);
    updateControls(data);
};

function updateControls (data) {
    $('#controls img').removeClass('hidden');

    if (data.page == 1) {
        $('#prev').addClass('hidden');
    }

    if (data.page == 9) {
        $('#next').addClass('hidden');
    }
}

function updateRepos (repos, data) {
    var repoElements = $('.repo');
    $.each(repos, function (index, repo) {
        var number = ((data.page - 1) * 10) + index +1;
        updateRepoDom($(repoElements[index]), repo, number);
    });
}

function updateRepoDom (element, repo, number) {
    element.children('.repo-title').html("<em>" + number + ". </em>" + repo["name"]);
    element.children('.goToGithub').attr("href", repo["html_url"]);
    element.children('.description').html(repo["description"]);
    element.children('.stars').html(repo["stargazers_count"] + " stars");

    if (repo.topContributor) {
        $(element.children('.topContributor')).removeClass('hidden');
        element.children('img').attr('src', repo.topContributor["avatar_url"]);
        element.children('.topContribrutor p').html("<a href=\"" + repo.topContributor["html_url"] + "</a> - " + repo.topContributor["contributions"] + " contributions");
    }
}

// Note: not all repos can list their top contributors because there are to many
// it seems to be common to then list the owner as top contributor