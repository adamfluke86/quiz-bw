/**
 * Created by adam on 10/19/16.
 */

function Data () {
    this.repos = [];
    this.page = 1;
}

Data.prototype.setRepos = function (repos) {
    if (!Array.isArray(repos)) {
        console.log('Type Check Failed Data.setRepos - array required', repos);
        return;
    }
    this.repos = repos;
};

Data.prototype.setTopContributor = function (contributor, config) {
    var data =this;
    if (!config || (config.index === undefined && config.id === undefined)) {
        console.log('config not valid - Data.setTopContributor requires config.id or config.index', config);
        return;
    }
    if (config.index !== undefined) {
        if(data.repos[config.index]){
            data.repos[config.index].topContributor = contributor;
        } else {
            console.log('config not valid - Data.setTopContributor config.index is a bad index', config);
        }
    }
};

Data.prototype.getCurrentRepos = function () {
    var data = this;
    var start = (data.page - 1) * 10;
    var end = start + 10;
    return data.repos.slice(start, end);
};

Data.prototype.shiftPage = function (step) {
    this.page = this.page + step;

    if (this.page <= 0) this.page = 1;
    if (this.page >= 10) this.page = 9;
};