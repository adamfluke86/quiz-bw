/**
 * Created by adam on 10/18/16.
 */
var GitHubWrapper;

(function () {
    var RepoURL = "https://api.github.com/search/repositories?q=stars:>=15000&sort=stars&order=desc";

    GitHubWrapper = {};

    GitHubWrapper.get = function (config) {
        return new Promise(function (resolve, reject) {
            if (config && config.repos) {
                $.get(RepoURL)
                    .then(function (response) {
                        resolve(response["items"])
                    })
                    .catch(function (response) {
                        console.log('GitHubWrapper.get failed - ', config, ' - ', response);
                        reject(response);
                    });
            } else if (config && config.topContributor) {
                $.get(config.contribUrl)
                    .then(function (response) {
                        resolve(response[0])
                    })
                    .catch(function (response) {
                        console.log('GitHubWrapper.get failed - ', config, ' - ', response);
                        reject(response)
                    });
            } else {
                console.log('Bad configuration sent to GitHubWrapper.get - ', config);
                reject({ message: "Bad config object" });
            }
        });
    };
})();

