*Brightwheel Exercise*

By: Adam Fluke
Date: October 15th, 2016

**Requirements**

Write a single page app that gets the top 100 starred projects on github,
and display them along with contributor information.

**Plan**

a. ***General:***

I considered using several different frameworks to make this app. I thought
of using one that I was familiar with like React or Angular 1.5, or
one I am learning like Angular 2 or Elm. However, I am a believer in using
the right tool for the right job, and all of them seem like overkill.
Plus, each in its own way pulls in dependencies, boilerplate code and
setup/configuration time. Normally this is fine because that cost is
recouped over the lifetime of the project, but since I anticipate this
project being done in 8 hours, that won't really happen this time.

As such, I have decided to do a simple MVC Javascript app that only pulls
in Jquery since it has some nice convenience methods for http calls and
DOM selection. If React, Angular or Elm are automatic milling machines,
Jquery is a Leatherman, and will be nice to have.

b. ***Outline of Services:***

    1. GitHubWrapper - the github wrapper will encapsilate the logic for
    making the http calls the the api and returning resources that will
    collectively constitue the Data (model)

    2. Data - an instance of this class will hold a reference to the projects
    returned by the github search, and the contributors, along with other
    state data (pagination, currently viewing project, etc)

    3. Render - a simple service that takes a model as input and renders
    the dom appropriately.

    4. Controller - an instance of this will instantiate a model, make
     the intitial calls to GitHubWrapper, and pass the model to the Render
     service whenever necessary.

c. ***Testing:***

I plan on using a simple Jasmine setup to test the public methods of the
services. Running the test suit will be as simple as opening tests/SpecRunner.html.